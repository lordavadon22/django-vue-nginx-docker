from django.urls import path
from rest_framework_jwt.views import (
    obtain_jwt_token,
    refresh_jwt_token,
    verify_jwt_token,
)

urlpatterns = [
    path(
        'auth/obtain_token/',
        obtain_jwt_token,
        name='api-jwt-auth'
    ),
    path(
        'auth/refresh_token/',
        refresh_jwt_token,
        name='api-jwt-refresh'
    ),
    path(
        'auth/verify_token/',
        verify_jwt_token,
        name='api-jwt-verify'
    ),
]
